@extends('layouts.auth')

@section('page-title')
View announcement #{{ $announcement->id }}
@endsection

@section('content')
	<div class = "row">
		<div class = "seven wide column">
			<div class = "ui small equal width text segment">
				<h4 class = "ui header">Edit announcement</h4>

				<form class = "ui small equal width form" action = "{{ route('announcements.update',$announcement->id) }}" method = "POST">
					{{ csrf_field() }}
					{{ method_field('PUT') }}

					<div class = "field">
						<label>Title</label>
						<input type = "text" id = "title" name = "title" value = "{{ $announcement->title }}" required>
					</div>

					<div class = "field">
						<label>Content</label>
						<textarea id = "content" name = "content" rows = "5" required>{{ $announcement->content }}</textarea>
					</div>

					<div class = "fields">
						<div class = "field">
							<label>Priority</label>
							<select id = "priority" name = "priority" class = "ui small fluid search dropdown" required>
								<option value = "LOW">LOW</option>
								<option value = "NORMAL">NORMAL</option>
								<option value = "HIGH">HIGH</option>
							</select>
						</div>

						<div class = "field">
							<label>Status</label>
							<select id = "status" name = "status" class = "ui small fluid search dropdown" required>
								<option value = "FOR REVIEW">FOR REVIEW</option>
								<option value = "APPROVED">APPROVED</option>
							</select>
						</div>
					</div>

					<div class = "field">
						<a href = "{{ route('announcements.index') }}" class = "ui small button">
							<i class = "left arrow icon"></i>
							Back
						</a>

						<button type = "submit" class = "ui small primary button">
							<i class = "send icon"></i>
							Submit
						</button>
					</div>
				</form>
			</div>
		</div>
	</div>
@endsection

@section('scripts')
	<script type = "text/javascript">
		$(document).ready(function(){
			$('#priority').dropdown('set selected', '{{ $announcement->priority }}');
			$('#status').dropdown('set selected', '{{ $announcement->status }}');
		});
	</script>
@endsection
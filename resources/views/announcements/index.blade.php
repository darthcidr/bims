@extends('layouts.auth')

@section('page-title')
Announcements
@endsection

@section('content')
	<div class = "row">
		<div class = "seven wide column">
			<div class = "ui small equal width text segment">
				<h4 class = "ui header">Add announcement</h4>

				@include('includes.notify')

				<form class = "ui small equal width form" action = "{{ route('announcements.store') }}" method = "POST">
					{{ csrf_field() }}

					<div class = "field">
						<label>Title</label>
						<input type = "text" id = "title" name = "title" required>
					</div>

					<div class = "field">
						<label>Content</label>
						<textarea id = "content" name = "content" rows = "5" required></textarea>
					</div>

					<div class = "field">
						<label>Priority</label>
						<select id = "priority" name = "priority" class = "ui small fluid search dropdown" required>
							<option value = "LOW">LOW</option>
							<option value = "NORMAL">NORMAL</option>
							<option value = "HIGH">HIGH</option>
						</select>
					</div>

					<div class = "field">
						<button type = "submit" class = "ui small primary button">
							<i class = "send icon"></i>
							Submit
						</button>
					</div>
				</form>
			</div>
		</div>

		<div class = "column">
			<div class = "ui small equal width text segment">
				<h4 class = "ui header">List of announcements</h4>

				<table id = "tblListOfAnnouncements" class = "ui small celled striped table" cellspacing="0" width="100%">
					<thead>
						<tr>
							<th>#</th>
							<th>Title</th>
							<th>Posted by</th>
							<th>Date posted</th>
							<th>Status</th>
							<th>Action</th>
						</tr>
					</thead>
				</table>
			</div>
		</div>
	</div>
@endsection
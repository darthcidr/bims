@extends('layouts.auth')

@section('page-title')
Dashboard
@endsection

@section('content')
	<div class = "row">
		<div class = "column">
			<div class = "ui equal width center aligned text segment">
				{!! $chart_residents_per_block->html() !!}
			</div>
		</div>

		<div class = "column">
			<div class = "ui equal width center aligned text segment">
				{!! $chart_residents_by_sex->html() !!}
			</div>
		</div>

		<div class = "column">
			<div class = "ui equal width center aligned text segment">
				{!! $chart_residents_by_age->html() !!}
			</div>
		</div>
	</div>

	<div class = "row">
		<div class = "column">
			<div class = "ui equal width center aligned text segment">
				{!! $chart_residences_per_block->html() !!}
			</div>
		</div>

		<div class = "column">
			<div class = "ui equal width center aligned text segment">
				{!! $chart_residents_per_residence->html() !!}
			</div>
		</div>

	</div>

	<div class = "row">
		<div class = "column">
			<div class = "ui small equal width text segment">
				{!! $chart_blotters_per_month->html() !!}
			</div>
		</div>
	</div>
@endsection

@section('charts-js')
	{!! $chart_residents_per_block->script() !!}
	{!! $chart_residents_by_sex->script() !!}
	{!! $chart_residents_by_age->script() !!}
	{!! $chart_residences_per_block->script() !!}
	{!! $chart_residents_per_residence->script() !!}
	{!! $chart_blotters_per_month->script() !!}
@endsection
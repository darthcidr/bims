@extends('layouts.auth')

@section('page-title')
	Database Manager
@endsection

@section('content')
	<div class = "row">
		<div class = "column">
			@include('includes.notify')
		</div>
	</div>

	<div class = "row">
		<div class = "column">
			<div class = "ui small equal width text segment">
				<h4 class = "ui header">Backup database</h4>

				<form class = "ui small equal width form" action = "{{ route('database.backup') }}" method = "POST">
					{{ csrf_field() }}

					<div class = "field">
						<label>File name</label>
						<input type = "text" id = "filename" name = "filename" required>
					</div>

					<div class = "field">
						<button type = "submit" class = "ui small primary button">
							<i class = "download icon"></i>
							Backup
						</button>
					</div>
				</form>
			</div>
		</div>

		<div class = "column">
			<div class = "ui small equal width text segment">
				<h4 class = "ui header">Restore database</h4>

				<form class = "ui small equal width form" action = "{{ route('database.restore') }}" method = "POST" enctype="multipart/form-data">
					{{ csrf_field() }}

					<div class = "field">
						<label>Database file</label>
						<input type = "file" id = "file" name = "file" accept=".sql" required>
					</div>

					<div class = "field">
						<button type = "submit" class = "ui small primary button">
							<i class = "upload icon"></i>
							Restore
						</button>
					</div>
				</form>
			</div>
		</div>
	</div>
@endsection

@section('scripts')
	@if(session()->has('file'))
		<script type = "text/javascript">
			window.open('{{ env('APP_URL') }}/storage/database/{{ session('file') }}')
		</script>
	@endif
@endsection
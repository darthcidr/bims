@extends('layouts.auth')

@section('page-title')
Users
@endsection

@section('content')
	<div class = "row">
		<div class = "seven wide column">
			<div class = "ui small equal width text segment">
				<h4 class = "ui header">Add user</h4>

				@include('includes.notify')

				<form class = "ui small equal width form" action = "{{ route('users.store') }}" method = "POST">
					{{ csrf_field() }}

					<div class = "fields">
						<div class = "field">
							<label>First name</label>
							<input type = "text" id = "fname" name = "fname" required autofocus>
						</div>

						<div class = "field">
							<label>Middle name</label>
							<input type = "text" id = "mname" name = "mname">
						</div>

						<div class = "field">
							<label>Last name</label>
							<input type = "text" id = "lname" name = "lname" required>
						</div>
					</div>

					<div class = "fields">
						<div id = "validate-existing-user" class = "field">
							<label>Username</label>
							<input type = "text" id = "username" name = "username" required>
						</div>

						<div class = "field">
							<label>Email address</label>
							<input type = "email" id = "email" name = "email" required>
						</div>
					</div>

					<div class = "fields">
						<div class = "field">
							<label>Password</label>
							<input type = "password" id = "password" name = "password" required>
						</div>

						<div class = "field">
							<label>Confirm password</label>
							<input type = "password" id = "confirm_password" name = "password_confirmation" required>
						</div>
					</div>

					<div class = "field">
						<label>Access level</label>
						<select id = "level" name = "level" class = "ui fluid search dropdown" required>
							<option value = "admin">Admin</option>
							<option value = "chairman">Chairman</option>
							<option value = "kagawad">Kagawad</option>
							<option value = "secretary">Secretary</option>
						</select>
					</div>

					<div class = "field">
						<button type = "submit" class = "ui small primary button">
							<i class = "send icon"></i>
							Submit
						</button>
					</div>
				</form>
			</div>
		</div>

		<div class = "column">
			<div class = "ui small equal width text segment">
				<h4 class = "ui header">List of users</h4>

				<table id = "tblListOfUsers" class = "ui small celled striped table" cellspacing="0" width="100%">
					<thead>
						<tr>
							<th>Username</th>
							<th>Name</th>
							<th>Access level</th>
							<th>Action</th>
						</tr>
					</thead>
				</table>
			</div>
		</div>
	</div>
@endsection

@section('scripts')

@endsection
@extends('layouts.auth')

@section('page-title')
{{ $user->fname }} {{ $user->lname }} ({!! '@' !!}{{ $user->username }})
@endsection

@section('content')
	<div class = "row">
		<div class = "seven wide column">
			<div class = "ui small equal width text segment">
				<h4 class = "ui header">Edit user</h4>

				@include('includes.notify')

				<form class = "ui small equal width form" action = "{{ route('users.update',$user->username) }}" method = "POST">
					{{ csrf_field() }}
					{{ method_field('PUT') }}

					<div class = "fields">
						<div class = "field">
							<label>First name</label>
							<input type = "text" id = "fname" name = "fname" value = "{{ $user->fname }}" required autofocus>
						</div>

						<div class = "field">
							<label>Middle name</label>
							<input type = "text" id = "mname" name = "mname" value = "{{ $user->mname }}">
						</div>

						<div class = "field">
							<label>Last name</label>
							<input type = "text" id = "lname" name = "lname" value = "{{ $user->lname }}" required>
						</div>
					</div>

					<div class = "field">
						<label>Access level</label>
						<select id = "level" name = "level" class = "ui fluid search dropdown" required>
							<option value = "admin">Admin</option>
							<option value = "chairman">Chairman</option>
							<option value = "kagawad">Kagawad</option>
							<option value = "secretary">Secretary</option>
						</select>
					</div>

					<div class = "field">
						<a href = "{{ route('users.index') }}" class = "ui small button">
							<i class = "left arrow icon"></i>
							Back
						</a>

						<button type = "submit" class = "ui small primary button">
							<i class = "send icon"></i>
							Submit
						</button>
					</div>
				</form>
			</div>
		</div>
	</div>
@endsection

@section('scripts')
	<script type = "text/javascript">
		$(document).ready(function(){
			$('#level').dropdown('set selected', '{{ $user->level }}');
		});
	</script>
@endsection
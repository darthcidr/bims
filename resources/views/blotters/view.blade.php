@extends('layouts.auth')

@section('page-title')
View blotter #{{ $blotter->id }}
@endsection

@section('content')
	<div class = "row">
		<div class = "seven wide column">
			<div class = "ui small equal width text segment">
				<h4 class = "ui header">Edit blotter report #{{ $blotter->id }}</h4>

				<form class = "ui small equal width form" action = "{{ route('blotters.update', $blotter->id) }}" method = "POST">
					{{ csrf_field() }}
					{{ method_field('PUT') }}

					@include('includes.notify')

					<div class = "field">
						<label>Status</label>
						@if(auth()->user()->level == "admin" || auth()->user()->level == "captain" || auth()->user()->level == "kagawad")
							<select id = "status" name = "status" class = "ui small fluid search dropdown">
								<option value = "FOR REVIEW">FOR REVIEW</option>
								<option value = "REVIEWED">REVIEWED</option>
								<option value = "RESOLVED">RESOLVED</option>
							</select>
						@else
							<select id = "status" name = "status" class = "ui small fluid search dropdown" readonly>
								<option value = "FOR REVIEW">FOR REVIEW</option>
								<option value = "REVIEWED">REVIEWED</option>
								<option value = "RESOLVED">RESOLVED</option>
							</select>
						@endif
					</div>

					<div class = "field">
						<label>Complainant</label>
						<input type = "text" value = "{{ $blotter->fname }} {{ $blotter->lname }} (House #{{ $blotter->house_number }})" readonly>
					</div>

					<div class = "field">
						<label>Block number</label>
						<input type = "number" id = "block" name = "block" min = "1" max = "{{ $brgy_blks }}" value = "{{ $blotter->block }}" required>
					</div>

					<div class = "field">
						<label>Complaint</label>
						<textarea id = "complaint" name = "complaint" rows = "5" required>{{ $blotter->complaint }}</textarea>
					</div>

					<div class = "field">
						<a href = "{{ route('blotters.index') }}" class = "ui small button">
							<i class = "left arrow icon"></i>
							Back
						</a>

						<button type = "submit" class = "ui small button">
							<i class = "send icon"></i>
							Submit
						</button>

						<button type = "reset" class = "ui small button">
							<i class = "refresh icon"></i>
							Reset fields
						</button>
					</div>
				</form>
			</div>
		</div>
	</div>
@endsection

@section('scripts')
	<script type = "text/javascript">
		$(document).ready(function(){
			$('#status').dropdown('set selected','{{ $blotter->status }}');
		});
	</script>
@endsection
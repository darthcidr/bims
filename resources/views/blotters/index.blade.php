@extends('layouts.auth')

@section('page-title')
Blotters
@endsection

@section('content')
	<div class = "row">
		<div class = "seven wide column">
			<div class = "ui small equal width text segment">
				<h4 class = "ui header">Add blotter</h4>

				@include('includes.notify')

				<form class = "ui small equal width form" action = "{{ route('blotters.store') }}" method = "POST">
					{{ csrf_field() }}

					<div class = "field">
						<label>Complainant</label>
						<select id = "complainant" name = "complainant" class = "ui small fluid search dropdown" required>

						</select>
					</div>

					<div class = "field">
						<label>Resident/s involved</label>
						<select id = "involved_residents" name = "involved_residents[]" class = "ui small fluid search dropdown" multiple>

						</select>
					</div>

					<div class = "field">
						<label>Block number</label>
						<input type = "number" id = "block" name = "block" min = "1" max = "{{ $brgy_blks }}" required>
					</div>

					<div class = "field">
						<label>Complaint</label>
						<textarea id = "complaint" name = "complaint" rows = "5" required></textarea>
					</div>

					<div class = "field">
						<div class = "ui checkbox">
							<input type = "checkbox" id = "make_announcement" name = "make_announcement" value = "true" checked>
							<label>Create announcment</label>
						</div>
					</div>

					<div class = "field">
						<button type = "submit" class = "ui small primary button">
							<i class = "send icon"></i>
							Submit
						</button>

						<button type = "reset" class = "ui small negative button">
							<i class = "refresh icon"></i>
							Reset fields
						</button>
					</div>
				</form>
			</div>
		</div>

		<div class = "column">
			<div class = "ui small equal width text segment">
				<h4 class = "ui header">List of blotters</h4>

				<table id = "tblListOfBlotters" class = "ui small celled striped table" cellspacing="0" width="100%">
					<thead>
						<tr>
							<th>ID</th>
							<th>Complainant</th>
							<th>Date filed</th>
							<th>Status</th>
							<th>Action</th>
						</tr>
					</thead>
				</table>
			</div>
		</div>
	</div>
@endsection
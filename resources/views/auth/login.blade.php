{{-- @extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Login</div>

                <div class="panel-body">
                    <form class="form-horizontal" method="POST" action="{{ route('login') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">Password</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Remember Me
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Login
                                </button>

                                <a class="btn btn-link" href="{{ route('password.request') }}">
                                    Forgot Your Password?
                                </a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
 --}}

<!DOCTYPE html>
<html lang = "{{ app()->getLocale() }}">
    <header>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>Administrator Login | {{ config('app.name') }}</title>

        <link rel="stylesheet" type="text/css" href="{{ asset('packages/semantic-ui/semantic.css') }}">
    </header>

    <body>
        <div class = "ui container" style="padding-top: 7rem;">
            <div class = "ui stackable equal width grid">
                <div class = "row">
                    <div class = "column"></div>

                    <div class = "seven wide column">
                        <div class = "ui small equal width piled segment">
                            <h3 class = "ui center aligned header">Web Administrator Login</h3>

                            <div class = "ui negative icon message">
                                <i class = "exclamation icon"></i>
                                <p>
                                    You've reached the Administrator's login page. Unless you're the Admin, this page is off-limits to limits to regular visitors. <a href = "{{ route('welcome') }}">Click here</a> to go back.
                                </p>
                            </div>

                            @if($errors->has('username'))
                                <div class = "ui negative message">
                                    <i class = "close icon"></i>
                                    <div class = "header">Login failed</div>
                                    <p>{{ $errors->first('username') }}</p>
                                </div>
                            @endif

                            <form class = "ui small equal width form" method = "post" action = "{{ route('login') }}">
                                {{ csrf_field() }}

                                <div class = "{{ $errors->has('username') ? 'error ' : '' }}field">
                                    <label>Username</label>
                                    <input type = "text" id = "username" name = "username" maxlength="16" required autofocus>
                                </div>

                                <div class = "{{ $errors->has('password') ? 'error ' : '' }}field">
                                    <label>Password</label>
                                    <input type = "password" id = "password" name = "password" required>
                                </div>

                                <div class = "field">
                                    <div class = "ui small toggle checkbox">
                                        <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}>
                                        <label>Keep me logged in</label>
                                    </div>
                                </div>

                                <button type = "submit" class = "ui small primary button">
                                    Login
                                </button>
                            </form>
                        </div>
                    </div>

                    <div class = "column"></div>
                </div>
            </div>
        </div>

        <script type="text/javascript" src="{{ asset('packages/jquery/jquery.js') }}"></script>
        <script type="text/javascript" src="{{ asset('packages/semantic-ui/semantic.js') }}"></script>
        <script type="text/javascript" src="{{ asset('js/core.js') }}"></script>
    </body>
</html>
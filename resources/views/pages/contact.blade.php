@extends('layouts/un-auth')

@section('page-title')
Contact us
@endsection

@section('content')
	<h4 class = "ui header">Contact us</h4>

	<p>
		<strong><i class = "phone icon"></i> Telephone Number:</strong> {{ $brgy_tel ?? 'Please check your system settings' }}
	</p>

	<p>
		<strong><i class = "mail icon"></i> Email Address:</strong> <a href = "mailto:{{ $brgy_mail ?? 'Please check your system settings' }}">{{ $brgy_mail ?? 'Please check your system settings' }}</a>
	</p>
@endsection
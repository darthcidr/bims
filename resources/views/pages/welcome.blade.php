@extends('layouts/un-auth')

@section('page-title')
Home
@endsection

@section('content')
	<h4 class = "ui header">Home</h4>

	<p>
		Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc venenatis efficitur tellus, eu congue mi malesuada non. Nullam quis ultrices elit. Aenean pellentesque lorem elit, in convallis felis viverra ac. Mauris elementum nibh in nibh feugiat, eu semper ipsum sodales. Fusce eu libero auctor, fringilla nunc quis, posuere justo. Vestibulum sed lacinia nisl, vitae pulvinar velit. Sed rutrum leo eget nulla dictum, ac tincidunt nulla tempor. Donec venenatis tristique libero ac imperdiet. Fusce augue orci, porttitor eu purus eget, consectetur pellentesque augue.
	</p>

	<p>
		Integer vel faucibus mauris, congue dignissim est. Curabitur vitae libero non leo feugiat feugiat nec eu diam. Integer imperdiet dolor at ligula posuere posuere. Aenean mauris odio, faucibus in consequat at, rhoncus vitae purus. Nunc non orci eros. Maecenas risus lorem, tincidunt eleifend porttitor ac, volutpat a ex. Vestibulum in pretium leo, vel luctus erat. Maecenas nec consequat nisi. Etiam nec turpis vitae turpis ultricies facilisis nec a mi. Cras magna justo, luctus sed arcu vel, sodales posuere dolor. Ut nec feugiat dui. Praesent sodales accumsan feugiat. Sed vitae elementum erat, eu venenatis felis.
	</p>

	<p>
		In rutrum turpis leo, a volutpat lectus efficitur id. Phasellus iaculis volutpat sapien, eu vestibulum quam auctor id. Aenean velit ipsum, sagittis ac elit id, varius posuere justo. Maecenas quis sem vitae lacus sodales efficitur sed vitae sem. Nunc dapibus, orci a porta mollis, turpis ante porttitor turpis, eu maximus turpis magna at nunc. Phasellus dui lacus, ornare vitae sapien sit amet, interdum gravida est. Vivamus faucibus arcu vitae tellus maximus viverra. Donec suscipit felis vitae commodo pellentesque. Maecenas vulputate vitae eros viverra lobortis. Vivamus et dignissim neque. Nulla vel feugiat lacus, sed facilisis massa. Praesent tempus, leo sit amet euismod finibus, lacus lacus hendrerit nisl, ac congue lorem mauris at libero. Maecenas sit amet dolor maximus, fringilla nulla vitae, interdum ante. Nam accumsan, orci sed ullamcorper fermentum, nisl velit aliquam ex, vitae condimentum nulla sapien vitae quam. Fusce id malesuada arcu, nec vehicula lacus.
	</p>

	<p>
		Phasellus in tincidunt dui. Cras vitae tempus nisl, at molestie metus. Etiam tempor quis sapien eu faucibus. Aenean in velit et erat rhoncus gravida sit amet vel nibh. Ut sed est bibendum, venenatis justo nec, volutpat est. Ut quis mi id libero rutrum blandit vel sit amet elit. Maecenas non ornare est, et porttitor nunc. Integer lacinia sapien id nulla maximus dignissim. Integer magna dui, blandit in sodales sit amet, volutpat et est. Fusce id posuere est. Vivamus accumsan feugiat augue, in dignissim risus volutpat sed. Etiam faucibus velit in dui varius varius. Phasellus sit amet consectetur libero. Donec arcu metus, interdum sed neque id, suscipit mollis diam.
	</p>

	<p>
		Sed at suscipit urna. Suspendisse potenti. Quisque non metus non nunc mollis euismod. Nam dapibus scelerisque nunc, ut vestibulum purus convallis eget. Nulla non porttitor ipsum, vitae tempus magna. Praesent nec hendrerit turpis, id aliquet neque. In et viverra augue. Nullam at nibh nisi. Praesent cursus odio tempus lectus sodales, eu rhoncus lorem vehicula. Vestibulum eget neque ipsum. Praesent sit amet dui vel eros ornare ultricies. Mauris placerat nisl elit, in efficitur magna imperdiet vel. Quisque sit amet turpis pulvinar sapien molestie maximus eu ac lacus. Curabitur pulvinar eleifend feugiat. Integer cursus dignissim enim eget accumsan.
	</p>
@endsection
@extends('layouts.auth')

@section('page-title')
View resident #{{ $resident->id }}
@endsection

@section('content')
	<div class = "row">
		<div class = "three wide column">
			<div class = "ui small segment">
				<img class = "ui fluid image" src = "{{ asset( 'images/'.($resident->image == null ? 'image.png':'residents/'.$resident->image) ) }}" >
				<img class = "ui fluid image" src = "data:image/png;base64, {!! base64_encode(QrCode::format('png')->size(100)->generate($resident->id)) !!}">
			</div>
		</div>

		<div class = "column">
			<div class = "ui small equal width text segment">
				<h4 class = "ui header">Edit resident</h4>

				@include('includes.notify')

				<form class = "ui small equal width form" action="{{ route('residents.update',$resident->id) }}" enctype="multipart/form-data" method="POST">
					{{ csrf_field() }}
					{{ method_field('PUT') }}

					<div class = "fields">
						<div class = "field">
							<label>Last name</label>
							<input type = "text" id = "lname" name = "lname" value = "{{ $resident->lname }}" required autofocus>
						</div>

						<div class = "field">
							<label>First name</label>
							<input type = "text" id = "fname" name = "fname" value = "{{ $resident->fname }}" required>
						</div>

						<div class = "field">
							<label>Middle name</label>
							<input type = "text" id = "mname" name = "mname" value = "{{ $resident->mname }}">
						</div>

						<div class = "field">
							<label>2x2 picture</label>
							<input type = "file" id = "image" name = "image" accept=".jpg, .png">
						</div>
					</div>

					<div class = "field">
						<label>Date of birth</label>
						<input type = "date" id = "birthdate" name = "birthdate" value = "{{ $resident->birthdate }}" required>
					</div>

					<div class = "fields">
						<div class = "field">
							<label>Sex</label>
							<select id = "sex" name = "sex" class = "ui fluid search dropdown" required>
								<option value = "MALE">MALE</option>
								<option value = "FEMALE">FEMALE</option>
							</select>
						</div>

						<div class = "field">
							<label>Civil status</label>
							<select id = "civil_status" name = "civil_status" class = "ui fluid search dropdown" required>
								<option value = "SINGLE">SINGLE</option>
								<option value = "MARRIED">MARRIED</option>
								<option value = "WIDOWED">WIDOWED</option>
								<option value = "LEGALLY SEPARATED">LEGALLY SEPARATED</option>
							</select>
						</div>
					</div>

					<div class = "fields">
						<div class = "field">
							<label>Mobile number</label>
							<input type = "text" id = "cp_number" name = "cp_number" value = "{{ $resident->cp_number }}" length = "11">
						</div>

						<div class = "field">
							<label>Landline number</label>
							<input type = "text" id = "phone_number" name = "phone_number" value = "{{ $resident->phone_number }}" length = "7">
						</div>
					</div>

					<div class = "field">
						<label>Email address</label>
						<input type = "email" id = "email" name = "email" value = "{{ $resident->email }}">
					</div>

					<div class = "field">
						<label>House number</label>
						<select id = "house_number" name = "house_number" class = "ui small fluid search dropdown" required>

						</select>
					</div>

					<div class = "field">
						<a href = "{{ route('residents.index') }}" class = "ui small button">
							<i class = "left arrow icon"></i>
							Back
						</a>

						<button type = "submit" class = "ui small primary button">
							<i class = "send icon"></i>
							Submit
						</button>
					</div>
				</form>
			</div>
		</div>
	</div>
@endsection

@section('scripts')
	<script type = "text/javascript">
		$('#sex').dropdown('set selected', '{{ $resident->sex }}');
		$('#civil_status').dropdown('set selected', '{{ $resident->civil_status }}');
	</script>
@endsection
@extends('layouts.auth')

@section('page-title')
Residents
@endsection

@section('content')
	<div class = "row">
		<div class = "seven wide column">
			<div class = "ui small equal width text segment">
				<h4 class = "ui header">Add resident</h4>

				@include('includes.notify')

				<form class = "ui small equal width form" action="{{ route('residents.store') }}" enctype="multipart/form-data" method="POST">
					{{ csrf_field() }}

					<div class = "fields">
						<div class = "field">
							<label>Last name</label>
							<input type = "text" id = "lname" name = "lname" required autofocus>
						</div>

						<div class = "field">
							<label>First name</label>
							<input type = "text" id = "fname" name = "fname" required>
						</div>

						<div class = "field">
							<label>Middle name</label>
							<input type = "text" id = "mname" name = "mname">
						</div>
					</div>

					<div class = "field">
						<label>2x2 picture</label>
						<input type = "file" id = "image" name = "image" accept=".jpg, .png" required>
					</div>

					<div class = "fields">
						<div class = "field">
							<label>Date of birth</label>
							<input type = "date" id = "birthdate" name = "birthdate" required>
						</div>
						
						<div class = "field">
							<label>Sex</label>
							<select id = "sex" name = "sex" class = "ui fluid search dropdown" required>
								<option value = "MALE">MALE</option>
								<option value = "FEMALE">FEMALE</option>
							</select>
						</div>

						<div class = "field">
							<label>Civil status</label>
							<select id = "civil_status" name = "civil_status" class = "ui fluid search dropdown" required>
								<option value = "SINGLE">SINGLE</option>
								<option value = "MARRIED">MARRIED</option>
								<option value = "WIDOWED">WIDOWED</option>
								<option value = "LEGALLY SEPARATED">LEGALLY SEPARATED</option>
							</select>
						</div>
					</div>

					<div class = "fields">
						<div class = "field">
							<label>Mobile number</label>
							<input type = "text" id = "cp_number" name = "cp_number" length = "11">
						</div>

						<div class = "field">
							<label>Landline number</label>
							<input type = "text" id = "phone_number" name = "phone_number" length = "7">
						</div>
					</div>

					<div class = "field">
						<label>Email address</label>
						<input type = "email" id = "email" name = "email">
					</div>

					<div class = "field">
						<label>House number</label>
						<select id = "house_number" name = "house_number" class = "ui small fluid search dropdown" required>

						</select>
					</div>

					<div class = "field">
						<button type = "submit" class = "ui small primary button">
							<i class = "send icon"></i>
							Submit
						</button>
					</div>
				</form>
			</div>
		</div>

		<div class = "column">
			<div class = "ui small equal width segment">
				<h4 class = "ui header">List of residents</h4>

				<table id = "tblListOfResidents" class = "ui small celled striped table" width="100%" cellspacing="0">
					<thead>
						<tr>
							<th>ID</th>
							<th>Name</th>
							<th>House number</th>
							<th>Action</th>
						</tr>
					</thead>
				</table>
			</div>
		</div>
	</div>
@endsection

@section('js-vars')

@endsection

@section('scripts')
	
@endsection
@extends('layouts.auth')

@section('page-title')
View residence #{{ $residence->house_number }}
@endsection

@section('content')
	<div class = "row">
		<div class = "seven wide column">
			<div class = "ui small equal width text segment">
				<h4 class = "ui header">Edit residence</h4>

				@include('includes.notify')

				<form class = "ui small equal width form" action = "{{ route('residences.update',$residence->house_number) }}" method = "POST">
					{{ csrf_field() }}
					{{ method_field('PUT') }}

					<div class = "fields">
						<div class = "field">
							<label>Block number</label>
							<input type = "number" id = "block" name = "block" min = "1" max = "{{ $brgy_blks }}" value = "{{ $residence->block }}" disabled>
						</div>

						<div class = "field">
							<label>House number</label>
							<input type = "text" id = "house_number" name = "house_number" value = "{{ $residence->house_number }}" maxlength="5" disabled>
						</div>
					</div>

					<div class = "fields">
						<div class = "field">
							<label>Owners</label>
							<select id = "owners" class = "ui small search fluid dropdown">

							</select>
						</div>

						<div class = "five wide field">
							<label>Current owner</label>
							<input type = "number" id = "owner" name = "owner" value = "{{ $residence->owner }}" readonly>
						</div>
					</div>

					<div class = "field">
						<button type = "button" class = "ui small button">
							<i class = "left arrow icon"></i>
							Back
						</button>

						<button type = "submit" class = "ui small primary button">
							<i class = "send icon"></i>
							Submit
						</button>
					</div>
				</form>
			</div>
		</div>
	</div>
@endsection

@section('scripts')
	<script type = "text/javascript">
		$(document).ready(function(){
			$(document).on('change','#owners',function(event){
				event.preventDefault();

				$('#owner').val( $('#owners option:selected').val() );
			});
		});
	</script>
@endsection
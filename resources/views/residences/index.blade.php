@extends('layouts.auth')

@section('page-title')
Residences
@endsection

@section('content')
	<div class = "row">
		<div class = "seven wide column">
			<div class = "ui small equal width text segment">
				<h4 class = "ui header">Add residence</h4>

				@include('includes.notify')

				<form class = "ui small equal width form" action = "{{ route('residences.store') }}" method = "POST">
					{{ csrf_field() }}

					<div class = "fields">
						<div class = "field">
							<label>Block number</label>
							<input type = "number" id = "block" name = "block" min = "1" max = "{{ $brgy_blks }}" required>
						</div>

						<div class = "field">
							<label>House number</label>
							<input type = "text" id = "house_number" name = "house_number" maxlength="5" required>
						</div>
					</div>

					<div class = "field">
						<button type = "submit" class = "ui small primary button">
							<i class = "send icon"></i>
							Submit
						</button>
					</div>
				</form>
			</div>
		</div>

		<div class = "column">
			<div class = "ui small equal width text segment">
				<h4 class = "ui header">List of residences</h4>

				<table id = "tblListOfResidences" class = "ui small celled striped table" cellspacing="0" width="100%">
					<thead>
						<tr>
							<th>Block</th>
							<th>House number</th>
							<th>Owner</th>
							<th>Action</th>
						</tr>
					</thead>
				</table>
			</div>
		</div>
	</div>
@endsection
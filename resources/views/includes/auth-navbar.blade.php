<div class = "ui large top fixed borderless stackable menu" style="padding-left: 15rem;">
    <a href = "{{ route('welcome') }}" class = "header item">
        <i class = "large laravel icon"></i>
        {{ config('app.name') }}
    </a>

    <div class = "right item">
    	Welcome, {{ auth()->user()->fname }} {{ auth()->user()->lname }}! You're logged in as {{ assert_a_an( ucwords(auth()->user()->level) ) }} {{ (auth()->user()->level) }}
    </div>
</div>
<div class = "four wide column">
    <div class = "ui grid">
        <div class = "row">
            <div class = "column">
                <div class = "ui small segment">
                    <h4 class = "ui header">Announcements</h4>

                    @if(count($all_announcements) > 0)
                        <div class = "ui relaxed bulleted list">
                            @foreach($all_announcements as $all_announcement)
                                <div class = "item">{{ $all_announcement->content }} (Posted by: {{ $all_announcement->fname }} {{ $all_announcement->lname }})</div>
                            @endforeach
                        </div>
                    @else
                        <p>No advisories found</p>
                    @endif
                </div>
            </div>
        </div>

        {{-- <div class = "row">
            <div class = "column">
                <div class = "ui small segment">
                    <h4 class = "ui header">Public Advisories</h4>

                    @if(count($high_priority_announcements) > 0)
                        <div class = "ui relaxed ordered list">
                            @foreach($high_priority_announcements as $high_priority_announcement)
                                <div class = "item">{{ $high_priority_announcement->content }} (Posted by: {{ $high_priority_announcement->fname }} {{ $high_priority_announcement->lname }})</div>
                            @endforeach
                        </div>
                    @else
                        <p>No advisories found</p>
                    @endif
                </div>
            </div>
        </div> --}}

        <div class = "row">
            <div class = "column">
                <div class = "ui small segment">
                    <h4 class = "ui header">Administrator Login</h4>
                    @if(Auth::check())
                        You are logged in, {{ auth()->user()->level }} {{ auth()->user()->fname }}.
                        <a href = "{{ route('dashboard') }}">
                            Go to Dashboard
                        </a>
                    @else
                        <a href = "{{ route('login') }}" class = "ui fluid primary button">
                            Click to login
                        </a>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@if($errors->any())
	<div class = "ui fluid negative message">
		<i class = "close icon"></i>
		<div class = "header">{{ count($errors->all()) }} {{ count($errors->all) > 1 ? "validations" : "validation" }} failed</div>
		<ul class = "list">
			@foreach($errors->all() as $error)
				<li class = "item">{{ $error }}</li>
			@endforeach
		</ul>
	</div>
@endif

@if(session()->has('type'))
	<div class = "ui {{ session('type') }} message">
		<i class = "close icon"></i>
		<div class = "header">{{ session('title') }}</div>
		<p>
			{{ session('message') }}
		</p>
	</div>
@endif
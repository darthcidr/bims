<div class = "ui visible inverted borderless vertical sidebar menu" style = "z-index: 1000;">
    <a href = "{{ route('dashboard') }}" class = "item">
        <i class = "chart bar icon"></i>
        Dashboard
    </a>

    <a href = "{{ route('announcements.index') }}" class = "item">
        <i class = "bullhorn icon"></i>
        Announcements
    </a>

    <a href = "{{ route('blotters.index') }}" class = "item">
        <i class = "quote left icon"></i>
        Blotters
    </a>

    @if(auth()->user()->level == "admin")
        <a href = "{{ route('database.index') }}" class = "item">
            <i class = "database icon"></i>
            Database Manager
        </a>
    @endif
    
    <a href = "{{ route('reports.index') }}" class = "item">
        <i class = "print icon"></i>
        Forms and Reports
    </a>

    @if(auth()->user()->level == "chairman" || auth()->user()->level == "admin")
        <a href = "{{ route('residences.index') }}" class = "item">
            <i class = "home icon"></i>
            Residences Registry
        </a>
        <a href = "{{ route('residents.index') }}" class = "item">
            <i class = "id card icon"></i>
            Residents Registry
        </a>
    @endif

    @if(auth()->user()->level == "admin")
        <a href = "{{ route('settings.index') }}" class = "item">
            <i class = "cog icon"></i>
            System Settings
        </a>

        <a href = "{{ route('users.index') }}" class = "item">
            <i class = "user icon"></i>
            User Manager
        </a>
    @endif
    
    <a href = "{{ route('logout') }}" class = "item">
        <i class = "sign-out icon"></i>
        Log out
    </a>
</div>'
<div class = "ui large top fixed stackable borderless menu">
    <a class = "header item" href = "#">
        <i class = "large laravel icon"></i>
        {{-- <span id = "lblBrgyName"></span> --}}
        {{ $brgy_name ?? 'Name has not been set yet' }}
    </a>

    <div class = "right menu">
        <a class = "item" href = "{{ route('welcome') }}">Home</a>
        <a class = "item" href = "{{ route('about') }}">About us</a>
        <a class = "item" href = "{{ route('transparency') }}">Transparency</a>
        <a class = "item" href = "{{ route('services') }}">Services</a>
        <a class = "item" href = "{{ route('contact') }}">Contact us</a>
    </div>
</div>
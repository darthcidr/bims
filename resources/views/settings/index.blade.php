@extends('layouts.auth')

@section('page-title')
System settings
@endsection

@section('content')
	<div class = "row">
		<div class = "six wide column">
			<div class = "ui small text segment">
				<h4 class = "ui header">Add system setting</h4>

				@include('includes.notify')

				<form id = "frmAddSystemSetting" class = "ui small equal width form" action = "{{ route('settings.store') }}" method="POST">
					{{ csrf_field() }}

					<div class = "field">
						<label>Key</label>
						<input type = "text" id = "key" name = "key" maxlength="9" required autofocus>
					</div>

					<div class = "field">
						<label>Description</label>
						<input type = "text" id = "description" name = "description" required>
					</div>

					<div class = "field">
						<label>Value</label>
						<textarea id = "value" name = "value" rows="3"></textarea>
					</div>

					<button type = "submit" class = "ui small primary button">
						<i class = "send icon"></i>
						Submit
					</button>
				</form>
			</div>
		</div>

		<div class = "column">
			<div class = "ui equal width text segment">
				<h4 class = "ui header">List of system settings</h4>

				<table id = "tblListOfSystemSettings" class = "ui small celled striped table" width="100%" cellspacing="0">
					<thead>
						<tr>
							<th>Key</th>
							<th>Description</th>
							<th>Value</th>
							<th>Action</th>
						</tr>
					</thead>
				</table>
			</div>
		</div>
	</div>
@endsection
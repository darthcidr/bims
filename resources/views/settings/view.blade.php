@extends('layouts.auth')

@section('page-title')
View system setting {{ $system_setting->key }}
@endsection

@section('content')
	<div class = "row">
		<div class = "six wide column">
			<div class = "ui small text segment">
				<h4 class = "ui header">Edit system setting</h4>

				@if($errors->any())
					<div class = "ui fluid negative message">
						<i class = "close icon"></i>
						<div class = "header">{{ count($errors->all()) }} validation/s failed</div>
						<ul class = "list">
							@foreach($errors->all() as $error)
								<li class = "item">{{ $error }}</li>
							@endforeach
						</ul>
					</div>
				@endif

				@if(session()->has('type'))
					<div class = "ui positive message">
						<i class = "close icon"></i>
						<div class = "header">{{ session('title') }}</div>
						<p>
							{{ session('message') }}
						</p>
					</div>
				@endif

				<form id = "frmEditSystemSetting" class = "ui small equal width form" action = "{{ route('settings.update',$system_setting->key) }}" method="POST">
					{{ csrf_field() }}
					{{ method_field('PUT') }}

					<div class = "field">
						<label>Description</label>
						<input type = "text" id = "description" name = "description" value = "{{ $system_setting->description }}" required>
					</div>

					<div class = "field">
						<label>Value</label>
						<textarea id = "value" name = "value" rows="3">{{ $system_setting->value }}</textarea>
					</div>

					<div class = "field">
						<a href = "{{ route('settings.index') }}" class = "ui small button">
							<i class = "left arrow icon"></i>
							Back
						</a>

						<button type = "submit" class = "ui small primary button">
							<i class = "edit icon"></i>
							Submit
						</button>
					</div>
				</form>
			</div>
		</div>
	</div>
@endsection
<!DOCTYPE html>
<html lang = "{{ app()->getLocale() }}">
    <header>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>@yield('page-title') | {{ $brgy_name ?? 'Name has not been set yet' }} - Official Website</title>

        <link rel="stylesheet" type="text/css" href="{{ asset('packages/packages.css') }}">
    </header>

    <body>
        @include('includes/un-auth-navbar')

        <div class = "ui container" style="padding-top: 5rem;">
            <div class = "ui stackable grid">

                <div class = "row">
                    @include('includes/side-content')

                    <div class = "twelve wide column">
                        <div class = "ui equal width segment">
                            @yield('content')
                        </div>
                    </div>
                </div>

                <div class = "row">
                    <div class = "column">
                        <div class = "ui mini center aligned basic segment">
                            Barangay 191 Zone 20, Copyright {{ date('Y') }} &copy;, All rights reserved&reg; | Developed and maintained with &hearts; by <a href = "https://bitbucket.org/darthcidr">darthcidr</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <script type="text/javascript" src="{{ asset('packages/jquery/jquery.js') }}"></script>
        <script type="text/javascript" src="{{ asset('packages/semantic-ui/semantic.js') }}"></script>
        <script type="text/javascript" src="{{ asset('packages/datatable/datatables.js') }}"></script>
        <script type="text/javascript" src="{{ asset('js/core.js') }}"></script>
        @yield('scripts')
    </body>
</html>
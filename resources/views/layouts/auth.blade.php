<!DOCTYPE html>
<html lang = "{{ app()->getLocale() }}">
    <header>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>@yield('page-title') | {{ config('app.name') }}</title>

        <link rel="stylesheet" type="text/css" href="{{ asset('packages/packages.css') }}">
        {!! Charts::styles() !!}
    </header>

    <body>
        @include('includes.auth-navbar')
        @include('includes.auth-sidebar')

        <div class = "ui fluid container" style="padding-left: 17rem; padding-right: 2rem; padding-top: 4rem;">
            <div class = "ui stackable equal width grid">
                @yield('content')
            </div>
        </div>

        <script type="text/javascript" src="{{ asset('packages/jquery/jquery.js') }}"></script>
        <script type="text/javascript" src="{{ asset('packages/semantic-ui/semantic.js') }}"></script>
        <script type="text/javascript" src="{{ asset('packages/datatables/datatables.js') }}"></script>
        <script type="text/javascript" src="{{ asset('js/core.js') }}"></script>
        <script type="text/javascript" src="{{ asset('js/admin/core.js') }}"></script>
        {!! Charts::scripts() !!}
        @yield('charts-js')
        @yield('scripts')
    </body>
</html>
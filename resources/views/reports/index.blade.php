@extends('layouts.auth')

@section('page-title')
Generate Forms and Reports
@endsection

@section('content')
	<div class = "row">
		<div class = "column">
			<div class = "ui small equal width text segment">
				<h4 class = "ui header">Request for Appearance</h4>

				<table id = "tblRequestForAppearance" class = "ui small celled striped table" cellspacing="0" width="100%">
					<thead>
						<tr>
							<th>ID</th>
							<th>Complainant</th>
							<th>Date filed</th>
							<th>Status</th>
							<th>Action</th>
						</tr>
					</thead>
				</table>
			</div>
		</div>

		<div class = "column">
			<div class = "ui small equal width text segment">
				<h4 class = "ui header">Barangay Certification</h4>

				<table id = "tblBrgyCertification" class = "ui small celled striped table" width="100%" cellspacing="0">
					<thead>
						<tr>
							<th>ID</th>
							<th>Name</th>
							<th>House number</th>
							<th>Action</th>
						</tr>
					</thead>
				</table>
			</div>
		</div>
	</div>
@endsection

@section('scripts')

@endsection
<!DOCTYPE html>
<html lang = "{{ app()->getLocale() }}">
	<head>
		<meta charset="utf-8">
	    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	    <meta name="viewport" content="width=device-width, initial-scale=1">

	    <title>Barangay Certification for Resident #{{ $resident->id }} | {{ config('app.name', 'Laravel') }}</title>


	    <style>
	    	header, .title {
	    		font-weight: bold;
	    		text-align: center;
	    	}
	    	.title {
	    		padding-top: 1rem;
	    		font-size: 20px;
	    	}
	    	.subheader, footer {
	    		font-size: 12px;
	    	}
	    	.salutation, .body, .signature {
	    		padding-top: 1rem;
	    		padding-bottom: 1rem;
	    	}
	    	th, td {
	    		padding: 0px;
	    	}
	    	footer {
	    		position: absolute;
	    		bottom: 0rem;
	    		left: 0rem;
	    		right: 0rem;
	    	}
		</style>
	</head>

	<body>
		<header class = "header">
			<div>REPUBLIC OF THE PHILIPPINES</div>
			<div class = "subheader">Office of the Barangay, {{ $brgy_name ?? 'Please check your system settings' }}</div>
			<div class = "subheader">{{ $brgy_addr ?? 'Please check your system settings' }}</div>
		</header>

		<div class = "title">
			C&nbsp;E&nbsp;R&nbsp;T&nbsp;I&nbsp;F&nbsp;I&nbsp;C&nbsp;A&nbsp;T&nbsp;I&nbsp;O&nbsp;N
		</div>

		<hr>

		<div class = "salutation">
			To whom it may concern,
		</div>

		<div class = "body">
			<p>
				This is to certify that resident {{ $resident->fname }} {{ $resident->mname ?? '' }} {{ $resident->lame }} of residence #{{ $residence->house_number }} located at Block {{ $residence->block_number }} is a bonafide resident of this barangay.
			</p>
		</div>

		<div class = "signature">
			<p>Respectfully yours,</p>
			<p>&nbsp;</p>
			<p>
				<div><strong>{{ $brgy_cptn ?? 'Please check your system settings' }}</strong></div>
				<div>Chairman</div>
				<div>{{ $brgy_name ?? 'Please check your system settings' }}</div>
			</p>
		</div>

		<footer class = "footer">
			<hr>
			<table cellspacing="0" width="100%">
				<tr>
					<td>
						<div>Office of the Barangay</div>
						<div>Telephone: {{ $brgy_tel ?? 'Please check your system settings' }}</div>
						<div>Email address: {{ $brgy_mail ?? 'Please check your system settings' }}</div>
						<div>Printed by: {{ auth()->user()->fname }} {{ auth()->user()->lname }}</div>
					</td>
					<td align="right">
						<img src="data:image/png;base64,{{ DNS1D::getBarcodePNG(sprintf('%07d', $resident->id), 'C39',1) }}">
					</td>
				</tr>
			</table>
		</footer>
	</body>
</html>
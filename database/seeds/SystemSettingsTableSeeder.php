<?php

use Illuminate\Database\Seeder;

class SystemSettingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('system_settings')->insert([
        	[
        		'key'			=>	'BRGY_NAME',
        		'description'	=>	'Barangay Name',
        		'value'			=>	'Barangay 191 Zone 20',
        	],
        	[
        		'key'			=>	'BRGY_ADDR',
        		'description'	=>	'Barangay Address',
        		'value'			=>	'Electrical Road, Pasay City 1301',
        	],
        	[
        		'key'			=>	'BRGY_CPTN',
        		'description'	=>	'Barangay Captain',
        		'value'			=>	null,
        	],
        	[
        		'key'			=>	'BRGY_KGW1',
        		'description'	=>	'Barangay Kagawad',
        		'value'			=>	null,
        	],
        	[
        		'key'			=>	'BRGY_KGW2',
        		'description'	=>	'Barangay Kagawad',
        		'value'			=>	null,
        	],
        	[
        		'key'			=>	'BRGY_KGW3',
        		'description'	=>	'Barangay Kagawad',
        		'value'			=>	null,
        	],
        	[
        		'key'			=>	'BRGY_KGW4',
        		'description'	=>	'Barangay Kagawad',
        		'value'			=>	null,
        	],
        	[
        		'key'			=>	'BRGY_KGW5',
        		'description'	=>	'Barangay Kagawad',
        		'value'			=>	null,
        	],
        	[
        		'key'			=>	'BRGY_KGW6',
        		'description'	=>	'Barangay Kagawad',
        		'value'			=>	null,
        	],
        	[
        		'key'			=>	'BRGY_KGW7',
        		'description'	=>	'Barangay Kagawad',
        		'value'			=>	null,
        	],
            [
                'key'           =>  'BRGY_BLKS',
                'description'   =>  'Number of blocks',
                'value'         =>  null,
            ],
            [
                'key'           =>  'BRGY_TEL',
                'description'   =>  'Telephone number',
                'value'         =>  null,
            ],
            [
                'key'           =>  'BRGY_MAIL',
                'description'   =>  'Email address',
                'value'         =>  null,
            ],
        ]);
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateResidentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('residents', function (Blueprint $table) {
            $table->increments('id');
            $table->string('image')->nullable();
            $table->string('lname',25);
            $table->string('fname',25);
            $table->string('mname',25)->nullable();
            $table->date('birthdate');
            $table->string('sex',6);
            $table->string('civil_status',25);
            $table->string('cp_number',11)->nullable();
            $table->string('phone_number',7)->nullable();
            $table->string('email')->nullable();
            $table->string('house_number',5);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('residents');
    }
}

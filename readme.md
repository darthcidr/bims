# Barangay Information Management System

## About BIMS
This system aims to provide Barangay 191 Zone 20 an efficient way of managing day-to-day information by implementing an information management system that will automate certain processes and store information of certain entities.

## Installation
1.	In the project directory, open a terminal and run the following commands:
	1.	$ composer install
	2.	$ php artisan migrate
	3.	$ php artisan db:seed
2.	To run this application in a development environment, copy the file **.env.example** in the root of the project directory to a fresh **.env** file by running this command:
	**$ cp .env.example .env**
3.	Finally, run this command to deploy the application:
	**$ php artisan serve**
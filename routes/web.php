<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//VISITOR PAGES
Route::get('/', 'PagesController@welcome')->name('welcome');
Route::get('/about', 'PagesController@about')->name('about');
Route::get('/transparency', 'PagesController@transparency')->name('transparency');
Route::get('/services', 'PagesController@services')->name('services');
Route::get('/contact', 'PagesController@contact')->name('contact');

// Auth::routes();
// Authentication Routes...
Route::get('/login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('/login', 'Auth\LoginController@login');
Route::get('/logout', 'Auth\LoginController@logout')->name('logout');

// Registration Routes...
Route::get('/register', 'Auth\RegisterController@showRegistrationForm')->name('register');
Route::post('/register', 'Auth\RegisterController@register');

// Password Reset Routes...
Route::get('/password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
Route::post('/password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
Route::get('/password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
Route::post('/password/reset', 'Auth\ResetPasswordController@reset');

//DASHBOARDS
Route::get('/dashboard', 'DashboardController@index')->name('dashboard');

Route::prefix('/settings')->name('settings.')->middleware('admin')->group(function(){
	Route::get('/','SystemSettingsController@index')->name('index');
	Route::get('/view/{id}','SystemSettingsController@show')->name('view');
	Route::post('/store','SystemSettingsController@store')->name('store');
	Route::put('/update/{id}', [
		'as'	=>	'update',
		'uses' 	=> 	'SystemSettingsController@update'
	]);
});

//JSON APIs
Route::prefix('/ajax')->name('ajax.')->group(function(){
	Route::prefix('/auth')->name('auth.')->middleware('admin')->group(function(){
		Route::prefix('/settings')->name('settings.')->group(function(){
			Route::get('/{filter}','SystemSettingsController@list');
		});
	});

	Route::prefix('/auth')->name('auth.')->middleware('kagawad')->group(function(){
		Route::prefix('/residents')->name('residents.')->group(function(){
			Route::get('/{filter}','ResidentsController@list');
		});

		Route::prefix('/residences')->name('residences.')->group(function(){
			Route::get('/{filter}','ResidencesController@list');
		});

		Route::prefix('/blotters')->name('blotters.')->group(function(){
			Route::get('/{filter}','BlottersController@list');
		});

		Route::prefix('/users')->name('users.')->group(function(){
			Route::get('/{filter}','UsersController@list');
		});
	});

	Route::prefix('/address')->name('address.')->group(function(){
		Route::get('/provinces', 'AddressesController@get_provinces')->name('provinces');
		Route::post('/citymuns', 'AddressesController@get_citymuns')->name('citymuns');
		Route::post('/brgys', 'AddressesController@get_brgys')->name('brgys');
	});

	Route::prefix('/announcements')->name('announcements.')->group(function(){
		Route::get('/{filter}','AnnouncementsController@list');
	});
});

//RESIDENTS
Route::prefix('/residents')->name('residents.')->middleware('chairman')->group(function(){
	Route::get('/','ResidentsController@index')->name('index');
	Route::post('/store','ResidentsController@store')->name('store');
	Route::get('/view/{resident}','ResidentsController@show')->name('show');
	Route::put('/update/{id}', [
		'as'	=>	'update',
		'uses' 	=> 	'ResidentsController@update'
	]);
});

//RESIDENCES
Route::prefix('/residences')->name('residences.')->middleware('chairman')->group(function(){
	Route::get('/','ResidencesController@index')->name('index');
	Route::post('/store','ResidencesController@store')->name('store');
	Route::get('/view/{residence}', 'ResidencesController@show')->name('show');
	Route::put('/update/{id}', [
		'as'	=>	'update',
		'uses' 	=> 	'ResidencesController@update'
	]);
});

//ANNOUNCEMENTS
Route::prefix('/announcements')->name('announcements.')->group(function(){
	Route::get('/','AnnouncementsController@index')->name('index')->middleware('kagawad');
	Route::post('/store','AnnouncementsController@store')->name('store')->middleware('kagawad');
	Route::get('/view/{announcement}','AnnouncementsController@show')->name('show')->middleware('kagawad');
	Route::put('/update/{id}', [
		'as'	=>	'update',
		'uses' 	=> 	'AnnouncementsController@update'
	])->middleware('chairman');
});

//BLOTTERS
Route::prefix('/blotters')->name('blotters.')->middleware('kagawad')->group(function(){
	Route::get('/','BlottersController@index')->name('index');
	Route::post('/store','BlottersController@store')->name('store');
	Route::get('/view/{blotter}','BlottersController@show')->name('show');
	Route::put('/update/{id}', [
		'as'	=>	'update',
		'uses' 	=> 	'BlottersController@update'
	]);
});

//USERS
Route::prefix('/users')->name('users.')->middleware('admin')->group(function(){
	Route::get('/','UsersController@index')->name('index');
	Route::post('/store','UsersController@store')->name('store');
	Route::get('/view/{user}','UsersController@show')->name('show');
	Route::put('/update/{user}', [
		'as'	=>	'update',
		'uses' 	=> 	'UsersController@update'
	]);
});

//DATABASE
Route::prefix('/manage/database')->name('database.')->middleware('admin')->group(function(){
	Route::get('/','DatabaseController@index')->name('index');
	Route::post('/backup','DatabaseController@backup')->name('backup');
	Route::post('/restore','DatabaseController@restore')->name('restore');
});

//GENERATE REPORTS
Route::prefix('/reports')->name('reports.')->group(function(){
	Route::get('/','ReportsController@index')->name('index');
	Route::get('/certification/{resident}','ResidentsController@brgy_certification_report')->middleware('kagawad');

	Route::prefix('/summons')->name('summons.')->middleware('kagawad')->group(function(){
		Route::get('/blotter/{blotter}', 'BlottersController@summons_report');
	});
});

//TEST ROUTES
Route::prefix('/development')->name('development.')->middleware('admin')->group(function(){
	Route::prefix('/features')->name('features.')->group(function(){
		Route::get('/qr',function(){
			return QrCode::size(250)->encoding('utf-8')->generate('test', public_path().'/images/residents/qr.png');
		});
	});
});
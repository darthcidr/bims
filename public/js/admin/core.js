$(document).ready(function(){
    //SYSTEM SETTINGS
    var tblListOfSystemSettings = $('#tblListOfSystemSettings').DataTable({
        processing: true,
        ajax:{
            url: '/ajax/auth/settings/all',
            dataSrc: '',
            serverside: true
        },
        columns: [
            {'data':'key'},
            {'data':'description'},
            {'data':'value'},
            {'data':null}
        ],
        columnDefs: [{
            targets: -1,
            data: null,
            defaultContent: '\
                <button type = "button" id="btnViewSystemSetting" class="ui mini icon button"><i class = "eye icon"></i></button>\
            ',
            orderable: false
        },{
            searchable: true,
            orderable: true,
            targets: 0
        }],
        pageLength: 5,
        deferRender: true
    });
    $('#tblListOfSystemSettings tbody').on('click', '#btnViewSystemSetting', function(event){
        var dataSelect = tblListOfSystemSettings.row( $(this).parents('tr') ).data();
        var id = dataSelect['key'];
        self.location = ('/settings/view/'+id);
    });

    //RESIDENTS
    var tblListOfResidents = $('#tblListOfResidents').DataTable({
        processing: true,
        ajax:{
            url: '/ajax/auth/residents/all',
            dataSrc: '',
            serverside: true
        },
        columns: [
            {'data':'id'},
            {'data':'fullname',
                render:function(data,type,fullname,meta){
                    return $.trim(fullname.lname + ', ' + fullname.fname + ' ' + fullname.mname);
                }
            },
            {'data':'house_number'},
            {'data':null}
        ],
        columnDefs: [{
            targets: -1,
            data: null,
            defaultContent: '\
                <button type = "button" id="btnViewResident" class="ui mini icon button"><i class = "eye icon"></i></button>\
            ',
            orderable: false
        },{
            searchable: true,
            orderable: true,
            targets: 0
        }],
        pageLength: 5,
        deferRender: true
    });
    $('#tblListOfResidents tbody').on('click', '#btnViewResident', function(event){
        var dataSelect = tblListOfResidents.row( $(this).parents('tr') ).data();
        var id = dataSelect['id'];
        self.location = ('/residents/view/'+id);
    });
    $.ajax({
        url: '/ajax/auth/residents/all',
        method: 'get',
        dataType: 'json',
        success: function (result){
            var array = [];
            array.push('<option value = "0" class = "disabled item">SELECT RESIDENT</option>');
            for (var i = 0 ; i < result.length ; i++) {
                array.push(
                    '<option value = "'+result[i]["id"]+'">'+result[i]["fname"]+' '+result[i]["lname"]+' (ID: '+result[i]["id"]+'), #'+result[i]["house_number"]+'</option>'
                );
            }
            $('#owners, #complainant, #involved_residents').html(array);
        }
    });
    var tblBrgyCertification = $('#tblBrgyCertification').DataTable({
        processing: true,
        ajax:{
            url: '/ajax/auth/residents/all',
            dataSrc: '',
            serverside: true
        },
        columns: [
            {'data':'id'},
            {'data':'fullname',
                render:function(data,type,fullname,meta){
                    return $.trim(fullname.lname + ', ' + fullname.fname + ' ' + fullname.mname);
                }
            },
            {'data':'house_number'},
            {'data':null}
        ],
        columnDefs: [{
            targets: -1,
            data: null,
            defaultContent: '\
                <button type = "button" id="btnPrintBrgyCertification" class="ui mini icon button"><i class = "print icon"></i></button>\
            ',
            orderable: false
        },{
            searchable: true,
            orderable: true,
            targets: 0
        }],
        pageLength: 5,
        deferRender: true
    });
    $('#tblBrgyCertification tbody').on('click', '#btnPrintBrgyCertification', function(event){
        var dataSelect = tblBrgyCertification.row( $(this).parents('tr') ).data();
        var id = dataSelect['id'];
        window.open('/reports/certification/'+id);
    });

    //RESIDENCES
    var tblListOfResidences = $('#tblListOfResidences').DataTable({
        processing: true,
        ajax:{
            url: '/ajax/auth/residences/all',
            dataSrc: '',
            serverside: true
        },
        columns: [
            {'data':'block'},
            {'data':'house_number'},
            {'data':'resident',
                render:function(data,type,owner,meta){
                    return $.trim( (owner.fname == null ? '' : owner.fname) + ' ' + (owner.lname == null ? '' : owner.lname) );
                }
            },
            {'data':null}
        ],
        columnDefs: [{
            targets: -1,
            data: null,
            defaultContent: '\
                <button type = "button" id="btnViewResidence" class="ui mini icon button"><i class = "eye icon"></i></button>\
            ',
            orderable: false
        },{
            searchable: true,
            orderable: true,
            targets: 0
        }],
        pageLength: 5,
        deferRender: true
    });
    $('#tblListOfResidences tbody').on('click', '#btnViewResidence', function(event){
        var dataSelect = tblListOfResidences.row( $(this).parents('tr') ).data();
        var id = dataSelect['house_number'];
        self.location = ('/residences/view/'+id);
    });
    $.ajax({
        url: '/ajax/auth/residences/all',
        method: 'get',
        dataType: 'json',
        success: function (result){
            var array = [];
            array.push('<option value = "0" class = "disabled item">SELECT RESIDENCE</option>');
            for (var i = 0 ; i < result.length ; i++) {
                array.push(
                    '<option value = "'+result[i]["house_number"]+'">Block '+result[i]["block"]+', #'+result[i]["house_number"]+'</option>'
                );
            }
            $('#house_number').html(array);
        }
    });

    //ANNOUNCEMENTS
    var tblListOfAnnouncements = $('#tblListOfAnnouncements').DataTable({
        processing: true,
        ajax:{
            url: '/ajax/announcements/all',
            dataSrc: '',
            serverside: true
        },
        columns: [
            {'data':'id'},
            {'data':'title'},
            {'data':'postedBy',
                render: function(data,type,postedBy,meta){
                    return $.trim(postedBy.fname + ' ' + postedBy.lname);
                }
            },
            {'data':'created_at'},
            {'data':'status'},
            {'data':null}
        ],
        columnDefs: [{
            targets: -1,
            data: null,
            defaultContent: '\
                <button type = "button" id="btnViewAnnouncement" class="ui mini icon button"><i class = "eye icon"></i></button>\
            ',
            orderable: false
        },{
            searchable: true,
            orderable: true,
            targets: 0
        }],
        pageLength: 5,
        deferRender: true
    });
    $('#tblListOfAnnouncements tbody').on('click', '#btnViewAnnouncement', function(event){
        var dataSelect = tblListOfAnnouncements.row( $(this).parents('tr') ).data();
        var id = dataSelect['id'];
        self.location = ('/announcements/view/'+id);
    });

    var tblListOfBlotters = $('#tblListOfBlotters').DataTable({
        processing: true,
        ajax:{
            url: '/ajax/auth/blotters/all',
            dataSrc: '',
            serverside: true
        },
        columns: [
            {'data':'id'},
            {'data':'complainant',
                render:function(data,type,complainant,meta){
                    return $.trim( (complainant.fname == null ? '' : complainant.fname) + ' ' + (complainant.lname == null ? '' : complainant.lname) );
                }
            },
            {'data':'created_at'},
            {'data':'status'},
            {'data':null}
        ],
        columnDefs: [{
            targets: -1,
            data: null,
            defaultContent: '\
                <button type = "button" id="btnViewResidence" class="ui mini icon button"><i class = "eye icon"></i></button>\
                <button type = "button" id="btnPrintSummonsReport" class="ui mini icon button"><i class = "print icon"></i></button>\
            ',
            orderable: false
        },{
            searchable: true,
            orderable: true,
            targets: 0
        }],
        pageLength: 5,
        deferRender: true
    });
    $('#tblListOfBlotters tbody').on('click', '#btnViewResidence', function(event){
        var dataSelect = tblListOfBlotters.row( $(this).parents('tr') ).data();
        var id = dataSelect['id'];
        self.location = ('/blotters/view/'+id);
    });
    $('#tblListOfBlotters tbody').on('click', '#btnPrintSummonsReport', function(event){
        var dataSelect = tblListOfBlotters.row( $(this).parents('tr') ).data();
        var id = dataSelect['id'];
        window.open('/reports/summons/blotter/'+id);
    });
    var tblRequestForAppearance = $('#tblRequestForAppearance').DataTable({
        processing: true,
        ajax:{
            url: '/ajax/auth/blotters/summons',
            dataSrc: '',
            serverside: true
        },
        columns: [
            {'data':'id'},
            {'data':'complainant',
                render:function(data,type,complainant,meta){
                    return $.trim( (complainant.fname == null ? '' : complainant.fname) + ' ' + (complainant.lname == null ? '' : complainant.lname) );
                }
            },
            {'data':'created_at'},
            {'data':'status'},
            {'data':null}
        ],
        columnDefs: [{
            targets: -1,
            data: null,
            defaultContent: '\
                <button type = "button" id="btnPrintSummonsReport" class="ui mini icon button"><i class = "print icon"></i></button>\
            ',
            orderable: false
        },{
            searchable: true,
            orderable: true,
            targets: 0
        }],
        pageLength: 5,
        deferRender: true
    });
    $('#tblRequestForAppearance tbody').on('click', '#btnPrintSummonsReport', function(event){
        var dataSelect = tblRequestForAppearance.row( $(this).parents('tr') ).data();
        var id = dataSelect['id'];
        window.open('/reports/summons/blotter/'+id);
    });

    //USERS
    var tblListOfUsers = $('#tblListOfUsers').DataTable({
        processing: true,
        ajax:{
            url: '/ajax/auth/users/all',
            dataSrc: '',
            serverside: true
        },
        columns: [
            {'data':'username'},
            {'data':'user',
                render:function(data,type,user,meta){
                    return $.trim( (user.fname == null ? '' : user.fname) + ' ' + (user.lname == null ? '' : user.lname) );
                }
            },
            {'data':'level'},
            {'data':null}
        ],
        columnDefs: [{
            targets: -1,
            data: null,
            defaultContent: '\
                <button type = "button" id="btnViewUser" class="ui mini icon button"><i class = "eye icon"></i></button>\
            ',
            orderable: false
        },{
            searchable: true,
            orderable: true,
            targets: 0
        }],
        pageLength: 5,
        deferRender: true
    });
    $('#tblListOfUsers tbody').on('click', '#btnViewUser', function(event){
        var dataSelect = tblListOfUsers.row( $(this).parents('tr') ).data();
        var id = dataSelect['username'];
        self.location = ('/users/view/'+id);
    });
    $(document).on('keyup','#username',function(event){
        event.preventDefault();
        $.ajax({
            url: '/ajax/auth/users/all',
            method: 'get',
            dataType: 'json',
            success: function(data){
                for (var i = 0 ; i < data.length ; data++) {
                    if ($('#username').val() === data[i]['username']) {
                        $('#validate-existing-user').addClass('error');
                    }
                    else {
                        $('#validate-existing-user').removeClass('error');
                    }
                }
            }
        });
    });
});
//ADDRESSES
function fnGetProvinces(province,citymun,brgy) {
	$.ajax({
		url: '/ajax/address/provinces',
		method: 'get',
		dataType: 'json',
		success: function (result){
			var array = [];
			array.push('<option class = "disabled item">SELECT PROVINCE</option>');
			for (var i = 0; i < result.length; i++) {
				array.push('<option value = "'+result[i]['provcode']+'">'+result[i]['provdesc']+'</option>');
			}
			province.html(array);
		}
	});
}
function fnGetCityMuns(province,citymun,brgy) {
	$.ajax({
		url: '/ajax/address/citymuns',
		headers: {
			'X-CSRF-TOKEN' : $('meta[name=csrf-token]').attr('content')
		},
		method: 'post',
		data: {
			'X-CSRF-TOKEN' : $('meta[name=csrf-token]').attr('content'),
			'province' : province.val()
		},
		dataType: 'json',
		success: function (result){
			var array = [];
			array.push('<option class = "disabled item">SELECT CITY/MUNICIPALITY</option>');
			for (var i = 0; i < result.length; i++) {
				array.push('<option value = "'+result[i]['citymuncode']+'">'+result[i]['citymundesc']+'</option>');
			}
			citymun.html(array);
		}
	});
}
function fnGetBrgys(province,citymun,brgy) {
	$.ajax({
		url: '/ajax/address/brgys',
		headers: {
			'X-CSRF-TOKEN' : $('meta[name=csrf-token]').attr('content')
		},
		method: 'post',
		data: {
			'X-CSRF-TOKEN' : $('meta[name=csrf-token]').attr('content'),
			'citymun' : citymun.val()
		},
		dataType: 'json',
		success: function (result){
			var array = [];
			array.push('<option class = "disabled item">SELECT BRGY.</option>');
			for (var i = 0; i < result.length; i++) {
				array.push('<option value = "'+result[i]['brgycode']+'">'+result[i]['brgydesc']+'</option>');
			}
			brgy.html(array);
		}
	});
}
fnGetProvinces( $('#birth_province'),$('#birth_citymun'),$('#birth_brgy') );
$(document).on('change','#birth_province',function(){
	fnGetCityMuns( $('#birth_province'),$('#birth_citymun'),$('#birth_brgy') );
});
$(document).on('change','#birth_citymun',function(){
	fnGetBrgys( $('#birth_province'),$('#birth_citymun'),$('#birth_brgy') );
});
$('#same_as_brgy_191').checkbox({
	onChecked: function(){
		$('#birth_province').dropdown('set selected','1376');
		setTimeout(function(){$('#birth_citymun').dropdown('set selected','137605');},500);
		setTimeout(function(){$('#birth_brgy').dropdown('set selected','137605191');},1000);
	}
});
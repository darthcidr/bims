<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Resident extends Model
{
    public $fillable = [
		'lname',
		'fname',
		'mname',
		'birthdate',
		'sex',
		'civil_status',
		'block_number',
		'house_number',
    ];
}

<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\View;

use App\SystemSetting;

class SystemVariablesServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        if (Schema::hasTable('system_settings')) {
            $brgy_name = SystemSetting::where('key','BRGY_NAME')->first();
            $brgy_addr = SystemSetting::where('key','BRGY_ADDR')->first();
            $brgy_cptn = SystemSetting::where('key','BRGY_CPTN')->first();
            $brgy_kgwd = SystemSetting::where('key','like','BRGY_KGW%')->get();
            $brgy_blks = SystemSetting::where('key','BRGY_BLKS')->first();
            $brgy_tel = SystemSetting::where('key','BRGY_TEL')->first();
            $brgy_mail = SystemSetting::where('key','BRGY_MAIL')->first();
            
            View::share([
                'brgy_name'                     =>  $brgy_name['value'],
                'brgy_addr'                     =>  $brgy_addr['value'],
                'brgy_cptn'                     =>  $brgy_cptn['value'],
                'brgy_kgwd'                     =>  $brgy_kgwd,
                'brgy_blks'                     =>  (int)$brgy_blks['value'],
                'brgy_tel'                      =>  $brgy_tel['value'],
                'brgy_mail'                      =>  $brgy_mail['value'],
            ]);
        }
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}

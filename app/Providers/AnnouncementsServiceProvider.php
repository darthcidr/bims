<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\View;

use App\Announcement;

class AnnouncementsServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        if (Schema::hasTable('announcements')) {
            $all_announcements = Announcement::
                                        join('users','announcements.user','=','users.username')
                                        ->get();
            $high_priority_announcements = Announcement::
                                            join('users','announcements.user','=','users.username')
                                            ->where('priority','HIGH')
                                            ->where('status','APPROVED')
                                            ->get();

            View::share([
                'all_announcements'             =>  $all_announcements,
                'high_priority_announcements'   =>  $high_priority_announcements,
            ]);
        }
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}

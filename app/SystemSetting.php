<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SystemSetting extends Model
{
	public $incrementing = false;
	protected $primaryKey = 'key';
}

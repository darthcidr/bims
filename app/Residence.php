<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Residence extends Model
{
	public $incrementing = false;
    protected $primaryKey = 'house_number';
}

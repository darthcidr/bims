<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class Kagawad
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::check()) {
            if (auth()->user()->level == "kagawad" || auth()->user()->level == "chairman" || auth()->user()->level == "admin") {
                return $next($request);
            }
        }

        return abort(403);
    }
}

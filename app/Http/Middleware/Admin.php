<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class Admin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::check()) {
            if (auth()->user()->level == "admin") {
                return $next($request);
            }
        }

        // return abort(403);
        return redirect('https://www.youtube.com/watch?v=dQw4w9WgXcQ');
    }
}

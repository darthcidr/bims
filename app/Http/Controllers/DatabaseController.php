<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class DatabaseController extends Controller
{
	public function index()
	{
		return view('admin.database');
	}

    public function backup(Request $request)
    {
		$strBackupDB = ("mysqldump ".env('DB_DATABASE','bims')." -u ".env('DB_USERNAME','root')." > ".storage_path('app/public/database/').$request["filename"].".sql");

		shell_exec($strBackupDB);

		return redirect()->route('database.index')->with([
			'type'      =>  'positive',
            'title'     =>  'Successful',
            'message'   =>  'Database backup successful',
			'file'		=>	$request["filename"].'.sql',
		]);
    }

    public function restore(Request $request)
    {
		$request->validate([
    		'file'	=>	'file|max:25600'
    	]);

    	$file_name = $request["file"]->getClientOriginalName();
        $request->file->storeAs('database',$file_name,'public');


		$strRestoreDB = "mysql -u ".env('DB_USERNAME','root')." -h ".env('DB_HOST','127.0.0.1')." ".env('DB_DATABASE','bims')." < ".storage_path('app/public/database/').$file_name;
		$executed = shell_exec($strRestoreDB);
		return redirect()->route('database.index')->with([
			'type'      =>  'positive',
		    'title'     =>  'Successful',
		    'message'   =>  'Database restore successful',
		]);
    }
}

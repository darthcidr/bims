<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\User;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('users.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'email' => 'required|string|email|max:255|unique:users',
            'username' => 'required|string|max:16|unique:users',
            'level' => 'required|string|max:25',
            'fname' => 'required|string|max:50',
            'mname' => 'nullable|string|max:50',
            'lname' => 'required|string|max:50',
            'password' => 'required|string|min:4|confirmed',
        ]);

        $user = new User;
        $user->email = $request["email"];
        $user->username = $request["username"];
        $user->level = $request["level"];
        $user->fname = $request["fname"];
        $user->mname = $request["mname"];
        $user->lname = $request["lname"];
        $user->password = bcrypt($request["password"]);
        $user->save();

        return redirect()->route('users.index')->with([
            'type'      =>  'positive',
            'title'     =>  'Successful',
            'message'   =>  'Resource save successful',
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($username)
    {
        $user = User::find($username);

        return view('users.view')->with([
            'user' =>   $user,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $username)
    {
        $request->validate([
            'level' => 'required|string|max:25',
            'fname' => 'required|string|max:50',
            'mname' => 'nullable|string|max:50',
            'lname' => 'required|string|max:50',
        ]);

        $user = User::find($username);

        // $user->email = $request["email"];
        // $user->username = $request["username"];

        $user->level = $request["level"];
        $user->fname = $request["fname"];
        $user->mname = $request["mname"];
        $user->lname = $request["lname"];
        $user->update();

        return redirect()->route('users.index')->with([
            'type'      =>  'positive',
            'title'     =>  'Successful',
            'message'   =>  'Resource update successful',
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function list($filter)
    {
        if ($filter === "all") {
            $users = User::select('username','fname','lname','level')->get();
        }

        return $users;
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Announcement;

class AnnouncementsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('announcements.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'title'  => 'required|string|max:100',
            'content'  => 'required|max:255',
            'priority'  => 'required|string|max:6',
        ]);

        $announcement = new Announcement;
        $announcement->title = $request["title"];
        $announcement->content = $request["content"];
        $announcement->priority = $request["priority"];
        $announcement->status = "FOR REVIEW";
        $announcement->user = \Auth::user()->username;
        $announcement->save();

        return redirect()->route('announcements.index')->with([
            'type'      =>  'positive',
            'title'     =>  'Successful',
            'message'   =>  'Resource save successful',
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $announcement = Announcement::find($id);

        return view('announcements.view')->with([
            'announcement'  => $announcement,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'title'  => 'required|string|max:100',
            'content'  => 'required|max:255',
            'priority'  => 'required|string|max:6',
        ]);

        $announcement = Announcement::find($id);
        $announcement->title = $request["title"];
        $announcement->content = $request["content"];
        $announcement->priority = $request["priority"];
        $announcement->status = $request["status"];
        $announcement->user = \Auth::user()->username;
        $announcement->save();

        return redirect()->route('announcements.index')->with([
            'type'      =>  'positive',
            'title'     =>  'Successful',
            'message'   =>  'Resource update successful',
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function list($filter)
    {
        if ($filter == "all") {
            $announcements = Announcement::
                                join('users','announcements.user','=','users.username')
                                ->select(
                                    'announcements.id',
                                    'announcements.title',
                                    'announcements.created_at',
                                    'announcements.status',
                                    'users.fname',
                                    'users.lname'
                                )
                                ->get();
        }

        return $announcements;
    }
}

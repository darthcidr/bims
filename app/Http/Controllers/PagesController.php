<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PagesController extends Controller
{
	public function welcome()
    {
    	return view('pages.welcome');
    }

    public function about()
	{
		return view('pages.about');
	}

	public function transparency()
	{
		return view('pages.transparency');
	}

	public function services()
	{
		return view('pages.services');
	}

	public function contact()
	{
		return view('pages.contact');
	}
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use PDF;

use App\Resident;
use App\Residence;

class ResidentsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('residents.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'lname'         => 'required|string|max:25',
            'fname'         => 'required|string|max:25',
            'mname'         => 'nullable|string|max:25',
            'image'         => 'image|dimensions:ratio=1/1',
            'birthdate'     => 'required|date',
            'sex'           => 'required|string|max:6',
            'civil_status'  => 'required|string|max:25',
            'cp_number'     => 'nullable|string|max:11',
            'phone_number'  => 'nullable|string|max:7',
            'email'         => 'nullable|email',
            'house_number'  => 'required|string|max:5',
        ]);

        $resident = new Resident;
        $resident->lname = $request["lname"];
        $resident->fname = $request["fname"];
        $resident->mname = $request["mname"];

        $imageName = $request["house_number"].'_'.$request["lname"].'_'.$request["fname"]. '.' .$request['image']->getClientOriginalExtension();
        $request["image"]->move(public_path('/images/residents'), $imageName);
        $resident->image = $imageName;

        $resident->birthdate = $request["birthdate"];
        $resident->sex = $request["sex"];
        $resident->civil_status = $request["civil_status"];
        $resident->cp_number = $request["cp_number"] === null ? null : $request["cp_number"];
        $resident->phone_number = $request["phone_number"] === null ? null : $request["phone_number"];
        $resident->email = $request["email"] === null ? null : $request["email"];
        $resident->house_number = $request["house_number"];
        $resident->save();

        return redirect()->route('residents.index')->with([
            'type'      =>  'positive',
            'title'     =>  'Successful',
            'message'   =>  'Resource save successful',
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $resident = Resident::find($id);

        return view('residents.view')->with([
            'resident'  =>  $resident,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'lname'         => 'required|string|max:25',
            'fname'         => 'required|string|max:25',
            'mname'         => 'nullable|string|max:25',
            'image'         => 'image|dimensions:ratio=1/1',
            'birthdate'     => 'required|date',
            'sex'           => 'required|string|max:6',
            'civil_status'  => 'required|string|max:25',
            'cp_number'     => 'nullable|string|max:11',
            'phone_number'  => 'nullable|string|max:7',
            'email'         => 'nullable|email',
            'house_number'  => 'required|string|max:5',
        ]);

        $resident = Resident::find($id);
        if ($request['image'] !== null) {
            $imageName = $request["house_number"].'_'.$request["lname"].'_'.$request["fname"]. '.' .$request['image']->getClientOriginalExtension();
            $request["image"]->move(public_path('/images/residents'), $imageName);
        }
        else {
            $imageName = $resident->image;
        }
        $resident->image = $imageName;
        $resident->lname = $request["lname"];
        $resident->fname = $request["fname"];
        $resident->mname = $request["mname"];
        $resident->birthdate = $request["birthdate"];
        $resident->sex = $request["sex"];
        $resident->civil_status = $request["civil_status"];
        $resident->cp_number = $request["cp_number"] === null ? null : $request["cp_number"];
        $resident->phone_number = $request["phone_number"] === null ? null : $request["phone_number"];
        $resident->email = $request["email"] === null ? null : $request["email"];
        $resident->house_number = $request["house_number"];
        $resident->update();

        return redirect()->route('residents.index')->with([
            'type'      =>  'positive',
            'title'     =>  'Successful',
            'message'   =>  'Resource update successful',
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function list($filter)
    {
        if ($filter == "all") {
            $residents = Resident::all();
        }

        return $residents;
    }

    public function brgy_certification_report($resident)
    {
        $resident = Resident::find($resident);
        $residence = Residence::find($resident->house_number);

        $pdf_blotter = PDF::loadView('reports.brgy-cert',[
            'resident' => $resident,
            'residence' => $residence,
        ])
        ->setPaper('a4','portrait')
        ->setOptions([
            'defaultFont' => 'Courier'
        ]);
        return $pdf_blotter->stream('Brgy_Cert_Resident_'.$resident->id.'_'.date('Y-m-d h:i:s').'.pdf');
    }
}

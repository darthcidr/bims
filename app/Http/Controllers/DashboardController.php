<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Charts;

use DB;
use App\Residence;
use App\Resident;
use App\Blotter;
use App\SystemSetting;

class DashboardController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $residences_count = Residence::all()->count();
        $residents_count = Resident::all()->count();

        //charts
        $obj_residences_per_block = Residence::all();
        $chart_residences_per_block = Charts::database($obj_residences_per_block, 'pie', 'highcharts')
                                        ->title('Residences per block')
                                        ->responsive(true)
                                        ->groupBy('block');
        $obj_residents_per_residence = Resident::all();
        $chart_residents_per_residence = Charts::database($obj_residents_per_residence, 'pie', 'highcharts')
                                            ->title('Residents per residence')
                                            ->responsive(true)
                                            ->groupBy('house_number');


        $obj_residents_per_block = Resident::
                                    join('residences','residents.house_number','=','residences.house_number')
                                    ->select(
                                        'residents.id',
                                        'residences.block_number'
                                    )
                                    ->get();
        $chart_residents_per_block = Charts::database($obj_residents_per_block, 'pie', 'highcharts')
                                        ->title('Residents per block')
                                        ->responsive(true)
                                        ->groupBy('block');
        $obj_residents_by_sex = Resident::all();
        $chart_residents_by_sex = Charts::database($obj_residents_by_sex, 'pie', 'highcharts')
                                        ->title('Residents according to sex')
                                        ->responsive(true)
                                        ->groupBy('sex');
        $obj_residents_under18 = Resident::whereRaw('FLOOR((CURDATE()-birthdate)/10000) < 18')->count();
        $obj_residents_19to45 = Resident::whereRaw('FLOOR((CURDATE()-birthdate)/10000) BETWEEN 18 AND 46')->count();
        $obj_residents_46to60 = Resident::whereRaw('FLOOR((CURDATE()-birthdate)/10000) BETWEEN 45 AND 61')->count();
        $obj_residents_60above = Resident::whereRaw('FLOOR((CURDATE()-birthdate)/10000) > 59')->count();
        $chart_residents_by_age = Charts::create('bar', 'highcharts')
                                        ->title('Residents according to age')
                                        ->responsive(true)
                                        ->values([$obj_residents_under18,$obj_residents_19to45,$obj_residents_46to60,$obj_residents_60above])
                                        ->labels(['Under 18','19 to 45','46 to 60','60 above']);

        $obj_blotters_filed_per_month = Blotter::whereYear('created_at',date('Y'))->where('status','FOR REVIEW')->get();
        $obj_blotters_reviewed_per_month = Blotter::whereYear('updated_at',date('Y'))->where('status','REVIEWED')->get();
        $obj_blotters_resolved_per_month = Blotter::whereYear('updated_at',date('Y'))->where('status','RESOLVED')->get();
        $chart_blotters_per_month = Charts::multiDatabase('bar', 'highcharts')
                                        ->dataset('For review', $obj_blotters_filed_per_month)
                                        ->dataset('Reviewed', $obj_blotters_reviewed_per_month)
                                        ->dataset('Resolved', $obj_blotters_resolved_per_month)
                                        ->title('Blotters filed per month for the year '.date('Y'))
                                        ->responsive(true)
                                        ->xAxisTitle('Month')
                                        ->yAxisTitle('Blotters filed')
                                        ->elementLabel('Blotters filed per month')
                                        ->aggregateColumn('status','count')
                                        // ->dateColumn('voucher_date')
                                        ->groupByMonth()
                                        ->labels(['January','February','March','April','May','June','July','August','September','October','November','December']);

        return view('admin.dashboard')->with([
            'residences_count'              => $residences_count,
            'residents_count'               => $residents_count,
            'chart_residences_per_block'    => $chart_residences_per_block,
            'chart_residents_per_residence' => $chart_residents_per_residence,
            'chart_residents_per_block'     => $chart_residents_per_block,
            'chart_residents_by_sex'        => $chart_residents_by_sex,
            'chart_residents_by_age'        => $chart_residents_by_age,
            'chart_blotters_per_month'      => $chart_blotters_per_month,
        ]);
    }
}

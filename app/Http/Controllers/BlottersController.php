<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use PDF;

use App\Blotter;
use App\InvolvedResident;
use App\Announcement;
use App\Resident;
use App\Residence;

class BlottersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('blotters.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'complainant'   =>  'required',
            'block'         =>  'required|integer',
            'complaint'     =>  'required|string',
        ]);

        $blotter = new Blotter;
        $blotter->complainant = $request["complainant"];
        $blotter->block = $request["block"];
        $blotter->complaint = $request["complaint"];
        $blotter->status = "FOR REVIEW";
        $blotter->save();
        $blotter_id = $blotter->id;

        if ($request["involved_residents"] !== null) {
            foreach ($request["involved_residents"] as $involved_resident) {
                $resident = new InvolvedResident;
                $resident->blotter = $blotter_id;
                $resident->resident = $involved_resident;
                $resident->save();
            }
        }

        if ($request["make_announcement"] === "true") {
            $resident_to_summon = Resident::find($request["complainant"]);
            $announcement = new Announcement;
            $announcement->title = "Blotter #".$blotter->id;
            $announcement->content = "This is to inform that resident ".$resident_to_summon->fname." ".$resident_to_summon->lname." of house number ".$resident_to_summon->house_number." is hereby requested to appear in the barangay office regarding a blotter report filed under #".$blotter->id.".";
            $announcement->priority = "HIGH";
            $announcement->status = "FOR REVIEW";
            $announcement->user = \Auth::user()->username;
            $announcement->save();

            if ($request["involved_residents"] !== null) {
                foreach ($request["involved_residents"] as $involved_resident) {
                    $resident_to_summon = Resident::find($involved_resident);
                    $announcement = new Announcement;
                    $announcement->title = "Blotter #".$blotter->id;
                    $announcement->content = "This is to inform that resident ".$resident_to_summon->fname." ".$resident_to_summon->lname." of house number ".$resident_to_summon->house_number." is hereby requested to appear in the barangay office regarding a blotter report filed under #".$blotter->id.".";
                    $announcement->priority = "HIGH";
                    $announcement->status = "FOR REVIEW";
                    $announcement->user = \Auth::user()->username;
                    $announcement->save();
                }
            }
        }

        return redirect()->route('blotters.index')->with([
            'type'      =>  'positive',
            'title'     =>  'Successful',
            'message'   =>  'Resource save successful',
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $blotter = Blotter::
                    join('residents','blotters.complainant','=','residents.id')
                    ->where('blotters.id',$id)
                    ->select(
                        'blotters.id',
                        'blotters.block',
                        'blotters.complaint',
                        'blotters.status',
                        'blotters.created_at',
                        'blotters.complainant',
                        'residents.fname',
                        'residents.lname',
                        'residents.house_number',
                        'residents.cp_number',
                        'residents.phone_number'
                    )
                    ->first();

        $involved_residents = InvolvedResident::
                                join('residents','involved_residents.resident','=','residents.id')
                                ->where('involved_residents.blotter',$blotter->id)
                                ->select(
                                    'residents.fname',
                                    'residents.lname',
                                    'residents.house_number',
                                    'residents.cp_number',
                                    'residents.phone_number'
                                )
                                ->get();

        return view('blotters.view')->with([
            'blotter'               =>  $blotter,
            'involved_residents'    =>  $involved_residents,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'block'     =>  'required|integer',
            'complaint' =>  'required|string',
        ]);

        $blotter = Blotter::find($id);
        $blotter->status = $request["status"];
        $blotter->block = "Block ".$request["block"];
        $blotter->complaint = $request["complaint"];
        $blotter->update();

        return redirect()->route('blotters.index')->with([
            'type'      =>  'positive',
            'title'     =>  'Successful',
            'message'   =>  'Resource update successful',
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function list($filter)
    {
        if ($filter == "all") {
            $blotters = Blotter::
                        join('residents','blotters.complainant','=','residents.id')
                        ->select(
                            'blotters.id',
                            'residents.fname',
                            'residents.lname',
                            'blotters.created_at',
                            'blotters.status'
                        )->get();
        }
        else if ($filter == "summons") {
            $blotters_complainant = Blotter::
                                    join('residents','blotters.complainant','=','residents.id')
                                    ->select(
                                        'blotters.id',
                                        'residents.fname',
                                        'residents.lname',
                                        'blotters.created_at',
                                        'blotters.status'
                                    );
            $blotters = InvolvedResident::
                        join('blotters','involved_residents.blotter','=','blotters.id')
                        ->join('residents','involved_residents.resident','=','residents.id')
                        ->select(
                            'blotters.id',
                            'residents.fname',
                            'residents.lname',
                            'blotters.created_at',
                            'blotters.status'
                        )
                        ->union($blotters_complainant)
                        ->get();
        }

        return $blotters;
    }

    public function summons_report($blotter_id)
    {
        $blotter = Blotter::find($blotter_id);
        $resident = Resident::find($blotter->complainant);
        $residence = Residence::find($resident->house_number);

        $pdf_blotter = PDF::loadView('reports.blotter',[
            'blotter' => $blotter,
            'resident' => $resident,
            'residence' => $residence,
        ])
        ->setPaper('a4','portrait')
        ->setOptions([
            'defaultFont' => 'Courier'
        ]);
        return $pdf_blotter->stream('Blotter_'.$blotter->id.'_'.date('Y-m-d h:i:s').'.pdf');

        // return view('reports.blotter')->with([
        //     'blotter'   =>  $blotter,
        //     'resident'  =>  $resident,
        // ]);
    }
}

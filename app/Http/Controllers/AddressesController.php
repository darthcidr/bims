<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use DB;

class AddressesController extends Controller
{
    public function get_provinces()
    {
    	$provinces = DB::select("
    		SELECT
    			provcode,
    			provdesc
    		FROM
    			refprovince;
    	");

    	return $provinces;
    }

    public function get_citymuns(Request $request)
    {
    	$citymuns = DB::select("
    		SELECT
    			citymuncode,
    			citymundesc
    		FROM
    			refcitymun
    		WHERE
    			provcode = '".$request["province"]."';
    	");

    	return $citymuns;
    }

    public function get_brgys(Request $request)
    {
    	$brgys = DB::select("
    		SELECT
    			brgycode,
    			brgydesc
    		FROM
    			refbrgy
    		WHERE
    			citymuncode = '".$request["citymun"]."';
    	");

    	return $brgys;
    }
}

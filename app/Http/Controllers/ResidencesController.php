<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Residence;

class ResidencesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('residences.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'house_number'  => 'required|string|max:5|unique:residences',
            'block_number'         => 'required|integer',
        ]);

        $residence = new Residence;
        $residence->block = "Block ".$request["block"];
        $residence->house_number = $request["house_number"];
        $residence->save();

        return redirect()->route('residences.index')->with([
            'type'      =>  'positive',
            'title'     =>  'Successful',
            'message'   =>  'Resource save successful',
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $residence = Residence::find($id);

        return view('residences.view')->with([
            'residence' =>  $residence,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'owner'  => 'required',
        ]);

        $residence = Residence::find($id);
        $residence->owner = $request["owner"];
        $residence->update();

        return redirect()->route('residences.index')->with([
            'type'      =>  'positive',
            'title'     =>  'Successful',
            'message'   =>  'Resource update successful',
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function list($filter)
    {
        if ($filter == "all") {
            $residences = Residence::
                            leftJoin('residents','residences.resident','=','residents.id')
                            ->select(
                                'residences.block_number',
                                'residences.house_number',
                                'residences.resident',
                                'residents.fname',
                                'residents.lname'
                            )
                            ->get();
        }

        return $residences;
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\SystemSetting;

class SystemSettingsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('settings.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'key' => 'required|string|max:9|unique:system_settings',
            'description' => 'required|string',
            'value' => 'required|string',
        ]);

        $system_setting = new SystemSetting;
        $system_setting->key = $request["key"];
        $system_setting->description = $request["description"];
        $system_setting->value = $request["value"];
        $system_setting->save();

        return redirect()->route('settings.index', $request["key"])->with([
            'type'      =>  'positive',
            'title'     =>  'Successful',
            'message'   =>  'Resource save successful',
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $system_setting = SystemSetting::find($id);

        return view('settings.view')->with([
            'system_setting'    =>  $system_setting,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'description' => 'required|string',
            'value' => 'string',
        ]);

        $system_setting = SystemSetting::find($id);
        $system_setting->description = $request["description"];
        $system_setting->value = $request["value"];
        $system_setting->update();

        return redirect()->route('settings.index')->with([
            'type'      =>  'positive',
            'title'     =>  'Successful',
            'message'   =>  'Resource update successful',
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function list($filter)
    {
        if ($filter == "all") {
            $system_settings = SystemSetting::all();
        }

        return $system_settings;
    }
}
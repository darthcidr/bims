<?php

function assert_a_an($word)
{
	if ($word[0] == 'A' || $word[0] == 'E' || $word[0] == 'I' || $word[0] == 'O' || $word[0] == 'U') {
		return "an";
	}

	return "a";
}